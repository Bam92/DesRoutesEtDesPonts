# À propos de la traduction française…

[Framasoft](https://framasoft.org./) est une association œuvrant pour la promotion et la diffusion du logiciel libre et de la culture libre dans une démarche d’éducation populaire. Parmi ses projets, Framalang a pour objectif de traduire et diffuser des articles sur le blog de l’association et des ouvrages publiés dans la collection Framabook.

Cette traduction de l’ouvrage de Nadia Eghbal a été diffusée dans une première version sur le [Framablog](https://framablog.org/) entre septembre 2016 et janvier 2017. Un partenariat entre Framasoft et OpenEdition a permis la co-édition de ce livre.

Nous remercions chaleureusement les traducteurs et traductrices du groupe Framalang :

Adélie, AFS, alien spoon, Anthony, Asta (Gatien Bovyn), astraia_spica, Bam92 (Abel Mbula), Bidouille, Bromind (Martin Vassor), Ced, dominix, Edgar Lori, flo, glissière de sécurité, goofy, goudron, Julien / Sphinx, jums, Laure, Luc, Lumibd, lyn, Mika, MO, Opsylac (Diane Ranville), pasquin, Penguin, peupleLà, Piup, roptat, Rozmador, salade, serici, teromene, Théo, urlgaga, woof, xi (Juliette Tibayrenc), xXx.

Nous remercions également Solenne Louis pour sa relecture attentive de la traduction initiale.
